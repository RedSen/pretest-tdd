package com.priceminister.account.implementation;

import com.priceminister.account.api.Account;
import com.priceminister.account.api.AccountRule;
import com.priceminister.account.exceptions.IllegalAmountException;
import com.priceminister.account.exceptions.IllegalBalanceException;


public class CustomerAccount implements Account {

    private Double balance;

    public CustomerAccount() {
        super();
        this.balance = 0.0;
    }

    public void add(Double addedAmount) throws IllegalAmountException {
        if (addedAmount > 0) {
            this.balance += addedAmount;
        }else{
            throw new IllegalAmountException(addedAmount);
        }
    }

    public Double getBalance() {
        return this.balance;
    }

    public Double withdrawAndReportBalance(Double withdrawnAmount, AccountRule rule)
            throws IllegalBalanceException {

        Double newBalance = this.balance - withdrawnAmount;
        if (!rule.withdrawPermitted(newBalance))
            throw new IllegalBalanceException(newBalance);
        this.balance = newBalance;
        return this.getBalance();
    }

}
