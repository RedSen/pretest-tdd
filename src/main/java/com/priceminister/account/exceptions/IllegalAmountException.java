package com.priceminister.account.exceptions;


public class IllegalAmountException extends Exception {

    private static final long serialVersionUID = -9204191749972551939L;

    public IllegalAmountException(Double illegalAmount) {
        super("Illegal amount: "+illegalAmount);
    }

}
