package com.priceminister.account;


import com.priceminister.account.api.Account;
import com.priceminister.account.api.AccountRule;
import com.priceminister.account.exceptions.IllegalAmountException;
import com.priceminister.account.exceptions.IllegalBalanceException;
import com.priceminister.account.implementation.CustomerAccount;
import com.priceminister.account.implementation.CustomerAccountRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


/**
 * Please create the business code, starting from the unit tests below.
 * Implement the first test, the develop the code that makes it pass.
 * Then focus on the second test, and so on.
 * <p>
 * We want to see how you "think code", and how you organize and structure a simple application.
 * <p>
 * When you are done, please zip the whole project (incl. source-code) and send it to recrutement-dev@priceminister.com
 */
public class CustomerAccountTest {

    private Account customerAccount;
    private AccountRule rule;
    private Double actualBalance;

    @Rule
    public ExpectedException exceptionRule= ExpectedException.none();

    @Before
    public void setUp() {
        customerAccount = new CustomerAccount();
        rule = new CustomerAccountRule();
        actualBalance = customerAccount.getBalance();
    }

    /**
     * Tests that an empty account always has a balance of 0.0, not a NULL.
     */
    @Test
    public void testAccountWithoutMoneyHasZeroBalance() {
        actualBalance = customerAccount.getBalance();
        assertNotNull(actualBalance);
        assertEquals(0.0, customerAccount.getBalance(), 0.1);
    }

    /**
     * Adds money to the account and checks that the new balance is as expected.
     */
    @Test
    public void testAddPositiveAmount() throws IllegalAmountException {
        customerAccount.add(10.0);
        assertEquals(10.0 + actualBalance, customerAccount.getBalance(), 0.1);
    }

    /**
     * Tests that an illegal withdrawal throws an exception
     * with the excepted message.
     */
    @Test()
    public void testWithdrawAndReportBalanceIllegalBalance() throws IllegalBalanceException {
        double illegalAmount = actualBalance - 100.0;
        exceptionRule.expect(IllegalBalanceException.class);
        exceptionRule.expectMessage(is("Illegal account balance: "+illegalAmount));
        customerAccount.withdrawAndReportBalance(100.0, rule);
    }

    /**
     * Test adding negative amount of money
     * and checks that the new balance is not affected.
     */
    @Test
    public void testAddNegativeAmount() throws IllegalAmountException {
        exceptionRule.expect(IllegalAmountException.class);
        exceptionRule.expectMessage(is("Illegal amount: -10.0"));
        customerAccount.add(-10.0);
        assertEquals(actualBalance, customerAccount.getBalance(), 0.1);
    }

    /**
     * Test withdraw in passing test case
     */
    @Test()
    public void testWithdrawAndReportPositifBalance() throws IllegalBalanceException, IllegalAmountException {
        customerAccount.add(100.0);
        Double newBalance = customerAccount.withdrawAndReportBalance(100.0, rule);
        assertEquals(actualBalance, newBalance, 0.1);
    }


    // Also implement missing unit tests for the above functionalities.

}
